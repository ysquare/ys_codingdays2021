import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import {Button} from "@material-ui/core";


export default function Header(props) {
    const {classes} = props;

    return <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
            <div>
                <Button>Dashboard Example Button</Button>
            </div>
        </Toolbar>
    </AppBar>
}