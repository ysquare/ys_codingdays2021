import React from "react";

import Grid from "@material-ui/core/Grid";

import './Dashboard.scss';
import {Card, CardHeader, CardContent} from "@material-ui/core";


const Dashboard = () => {
    return <div>
        <Grid container className="grid">
            <Grid item xs={12} sm={6} md={3}>
                <Card>
                    <CardHeader title="Header"/>
                    <CardContent>
                        Content
                    </CardContent>
                </Card>
            </Grid>
        </Grid>
    </div>
}

export default Dashboard;
