import {AUTH_TOKEN} from "../../constants";

const isLoggedIn = () => localStorage.getItem(AUTH_TOKEN);

export {
    isLoggedIn
};